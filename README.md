# Backend 101

The is the companion repository of the Backend 101 blog series: where I cover essential topics in backend
development with practical guides suitable for readers of all level, such as:
- Designing effective REST APIs
- A guide to OpenAPI and API first approach
- Getting things done incrementally with Test Driven Development
- Practical Hexagonal Architecture
- More

For out the index page for linkages to these post: https://medium.com/@briannqc/backend-101-introduction-2e3c2ef448
