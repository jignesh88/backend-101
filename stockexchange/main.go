package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/in/web"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/out/auth"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/out/email"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/out/mem"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/out/sqlite"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/service"
)

func main() {
	engine := gin.Default()
	v1Api := engine.Group("/api/v1")

	sqliteDB := sqlx.MustOpen("sqlite3", ":memory:")
	sqliteDB.MustExec(`CREATE TABLE stock_order (id TEXT PRIMARY KEY, symbol TEXT, quantity INT, price_cent INT, side TEXT, status TEXT);`)

	orderPersistenceAdapter := sqlite.NewOrderPersistenceAdapter(sqliteDB)
	orderBookAdapter := mem.NewOrderBookAdapter()
	reportExecutionAdapter := email.NewReportExecutionAdapter()

	placeOrderService := service.NewPlaceOrderService(
		domain.GenerateOrderID,
		orderPersistenceAdapter,
		orderBookAdapter,
		reportExecutionAdapter,
	)
	placeOrderHandler := web.NewPlaceOrderHandler(placeOrderService)
	v1Api.POST("/orders", placeOrderHandler.HandlePlaceOrder)

	authorizationAdapter := &auth.StubAuthorizationAdapter{}
	getOrderService := service.NewGetOrderService(authorizationAdapter, orderPersistenceAdapter)
	getOrderHandler := web.NewGetOrderHandler(getOrderService)
	v1Api.POST("/orders/:id", getOrderHandler.HandleGetOrder)

	err := engine.Run(":8080")
	if err != nil {
		log.Fatal("Starting server failed", err)
	}
}
