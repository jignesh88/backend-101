package email

import (
	"context"
	"log"

	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

type ReportExecutionAdapter struct {
}

func NewReportExecutionAdapter() *ReportExecutionAdapter {
	return &ReportExecutionAdapter{}
}

func (a *ReportExecutionAdapter) ReportExecution(ctx context.Context, orders ...domain.Order) {
	for _, o := range orders {
		log.Printf("Sending execution report email for order id: %v, status: %v\n", o.ID(), o.Status())
	}
}
