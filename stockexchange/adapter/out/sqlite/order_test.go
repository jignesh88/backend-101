package sqlite_test

import (
	"context"
	"database/sql"
	"testing"

	_ "github.com/mattn/go-sqlite3"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/out/sqlite"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain/stub"
)

func mustNewInMemDB() *sqlx.DB {
	db := sqlx.MustOpen("sqlite3", ":memory:")
	db.MustExec(`CREATE TABLE stock_order (id TEXT PRIMARY KEY, symbol TEXT, quantity INT, price_cent INT, side TEXT, status TEXT);`)
	return db
}

func TestOrderPersistenceAdapter_InsertOrder(t *testing.T) {
	tests := []struct {
		name    string
		order   domain.Order
		wantErr error
	}{
		{
			name:    "GIVEN insert order THEN order is in db",
			order:   stub.BuyOrderNew(),
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := mustNewInMemDB()
			a := sqlite.NewOrderPersistenceAdapter(db)
			ctx := context.Background()
			gotErr := a.InsertOrder(ctx, tt.order)
			assert.ErrorIs(t, gotErr, tt.wantErr)

			wantInsertedOrder := sqlite.Order{
				ID:        tt.order.ID(),
				Symbol:    string(tt.order.Symbol()),
				Quantity:  tt.order.Quantity(),
				PriceCent: uint64(tt.order.Price()),
				Side:      string(tt.order.Side()),
				Status:    string(tt.order.Status()),
			}
			var gotInsertedOrders []sqlite.Order
			err := db.Select(&gotInsertedOrders, "SELECT id, symbol, quantity, price_cent, side, status FROM stock_order")
			assert.NoError(t, err)
			assert.ElementsMatch(t, gotInsertedOrders, []sqlite.Order{wantInsertedOrder})
		})
	}
}

func TestOrderPersistenceAdapter_UpdateAllOrders(t *testing.T) {
	tests := []struct {
		name    string
		orders  []domain.Order
		wantErr error
	}{
		{
			name:    "GIVEN update all orders THEN orders in db are updated",
			orders:  []domain.Order{stub.BuyOrderFilled(), stub.SellOrderFilled()},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := mustNewInMemDB()
			a := sqlite.NewOrderPersistenceAdapter(db)
			ctx := context.Background()

			var wantInsertedOrder []sqlite.Order
			for _, o := range tt.orders {
				wantInsertedOrder = append(wantInsertedOrder, sqlite.Order{
					ID:        o.ID(),
					Symbol:    string(o.Symbol()),
					Quantity:  o.Quantity(),
					PriceCent: uint64(o.Price()),
					Side:      string(o.Side()),
					Status:    string(o.Status()),
				})

				o.SetStatus(domain.StatusNew)
				err := a.InsertOrder(ctx, o)
				assert.NoError(t, err)
			}

			gotErr := a.UpdateAllOrders(ctx, tt.orders...)
			assert.ErrorIs(t, gotErr, tt.wantErr)

			var gotInsertedOrders []sqlite.Order
			err := db.Select(&gotInsertedOrders, "SELECT id, symbol, quantity, price_cent, side, status FROM stock_order")
			assert.NoError(t, err)
			assert.ElementsMatch(t, gotInsertedOrders, wantInsertedOrder)
		})
	}
}

func TestOrderPersistenceAdapter_GetOrderByID(t *testing.T) {
	anOrder := stub.BuyOrderNew()
	anotherOrder := stub.SellOrderFilled()
	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name       string
		args       args
		seedOrders []domain.Order
		want       domain.Order
		wantErr    error
	}{
		{
			name: "GIVEN order not exist THEN returns sql: no rows in result set",
			args: args{
				ctx: context.Background(),
				id:  anOrder.ID(),
			},
			seedOrders: nil,
			wantErr:    sql.ErrNoRows,
		},
		{
			name: "GIVEN order exists THEN returns the order",
			args: args{
				ctx: context.Background(),
				id:  anOrder.ID(),
			},
			seedOrders: []domain.Order{anOrder, anotherOrder},
			want:       anOrder,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := mustNewInMemDB()
			a := sqlite.NewOrderPersistenceAdapter(db)
			for _, order := range tt.seedOrders {
				err := a.InsertOrder(tt.args.ctx, order)
				if err != nil {
					t.Fatal("Seeding orders failed", err)
				}
			}

			got, err := a.GetOrderByID(tt.args.ctx, tt.args.id)
			if tt.wantErr != nil {
				assert.ErrorIs(t, err, tt.wantErr)
			} else {
				assert.Equalf(t, tt.want, got, "GetOrderByID(%v, %v)", tt.args.ctx, tt.args.id)
			}
		})
	}
}
