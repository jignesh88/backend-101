package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/in"
)

type PlaceOrderHandler struct {
	uc in.PlaceOrderUseCase
}

func NewPlaceOrderHandler(uc in.PlaceOrderUseCase) *PlaceOrderHandler {
	return &PlaceOrderHandler{uc: uc}
}

// HandlePlaceOrder handles HTTP request to place an order.
// The method name must be in its long-form while it can be
// shortened to say "Handle", given its enclosing scope because
// metric emitter uses function names to distinguish endpoints.
func (h *PlaceOrderHandler) HandlePlaceOrder(c *gin.Context) {
	req := struct {
		Symbol   string
		Quantity uint
		Price    float64
		Side     string
	}{}
	if err := c.ShouldBindJSON(&req); err != nil {
		resp := newErrorResponse(
			"ERR1001",
			"Request body is not a proper JSON",
			"Your place order request has been received at our end, but we could not understand it due to improper format. Please modify your request following our API spec and try again. If the issue keeps happening, contact our Customer Support.",
		)
		c.JSON(http.StatusBadRequest, resp)
		return
	}

	id, err := h.uc.PlaceOrder(c, req.Symbol, req.Quantity, uint64(req.Price*100), req.Side)
	if err != nil {
		c.JSON(statusCodeOf(err), errorResponseOf(err))
		return
	}
	resp := newSuccessfulResponse(
		"Order is placed successfully",
		gin.H{"id": id},
	)
	c.JSON(http.StatusAccepted, resp)
}
