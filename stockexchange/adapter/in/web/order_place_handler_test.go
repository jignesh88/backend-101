package web_test

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/in/web"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/in"
)

func TestPlaceOrderHandler_HandlePlaceOrder(t *testing.T) {
	type UCArgs struct {
		symbol    string
		quantity  uint
		priceCent uint64
		side      string
	}
	type UCReturn struct {
		id  string
		err error
	}
	tests := []struct {
		name             string
		requestBody      string
		ucArgs           UCArgs
		ucReturn         UCReturn
		wantStatusCode   int
		wantResponseBody string
	}{
		{
			name:           "GIVEN request body is not json THEN return 400",
			requestBody:    "not a json string",
			wantStatusCode: http.StatusBadRequest,
			wantResponseBody: `{
				"errorCode": "ERR1001",
				"message": "Request body is not a proper JSON",
				"details": "Your place order request has been received at our end, but we could not understand it due to improper format. Please modify your request following our API spec and try again. If the issue keeps happening, contact our Customer Support."
			}`,
		},
		{
			name: "GIVEN use case returns known error THEN return relevent error",
			requestBody: `{
				"symbol": "apple",
				"quantity": 100,
				"price": 123.45,
				"side": "BUY"
			}`,
			ucArgs: UCArgs{
				symbol:    "apple",
				quantity:  100,
				priceCent: 12345,
				side:      "BUY",
			},
			ucReturn: UCReturn{
				err: domain.ErrInvalidStockSymbol,
			},
			wantStatusCode: http.StatusBadRequest,
			wantResponseBody: fmt.Sprintf(
				`{"errorCode": "%v", "message": "%v", "details": "%v"}`,
				domain.ErrInvalidStockSymbol.Code(),
				domain.ErrInvalidStockSymbol.Message(),
				domain.ErrInvalidStockSymbol.Details(),
			),
		},
		{
			name: "GIVEN use case returns known error THEN return relevent error",
			requestBody: `{
				"symbol": "AAPL",
				"quantity": 100,
				"price": 123.45,
				"side": "BUY"
			}`,
			ucArgs: UCArgs{
				symbol:    "AAPL",
				quantity:  100,
				priceCent: 12345,
				side:      "BUY",
			},
			ucReturn: UCReturn{
				err: errors.New("unrecognized error"),
			},
			wantStatusCode: http.StatusInternalServerError,
			wantResponseBody: `{
				"errorCode": "ERR0000",
				"message":   "Something wrong happened at our end",
				"details":   "Please try again after awhile. If the issue keeps happening, contact our Customer Support."
			}`,
		},
		{
			name: "GIVEN use case returns error THEN return 202",
			requestBody: `{
				"symbol": "MSFT",
				"quantity": 100,
				"price": 123.00,
				"side": "SELL"
			}`,
			ucArgs: UCArgs{
				symbol:    "MSFT",
				quantity:  100,
				priceCent: 12300,
				side:      "SELL",
			},
			ucReturn: UCReturn{
				id: "gen-id",
			},
			wantStatusCode:   http.StatusAccepted,
			wantResponseBody: `{"message": "Order is placed successfully", "data": {"id": "gen-id"}}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			uc := in.NewMockPlaceOrderUseCase(ctrl)
			uc.EXPECT().
				PlaceOrder(gomock.Any(), tt.ucArgs.symbol, tt.ucArgs.quantity, tt.ucArgs.priceCent, tt.ucArgs.side).
				Return(tt.ucReturn.id, tt.ucReturn.err).
				MaxTimes(1)

			h := web.NewPlaceOrderHandler(uc)

			router := gin.Default()
			router.POST("/orders", h.HandlePlaceOrder)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, "/orders", strings.NewReader(tt.requestBody))

			router.ServeHTTP(w, req)

			assert.Equal(t, tt.wantStatusCode, w.Code)
			assert.JSONEq(t, tt.wantResponseBody, w.Body.String())
		})
	}
}
