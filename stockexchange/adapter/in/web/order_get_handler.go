package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/in"
)

type GetOrderHandler struct {
	uc in.GetOrderUseCase
}

func NewGetOrderHandler(uc in.GetOrderUseCase) *GetOrderHandler {
	return &GetOrderHandler{uc: uc}
}

func (h *GetOrderHandler) HandleGetOrder(c *gin.Context) {
	order, err := h.uc.GetOrder(c, c.Param("id"))
	if err != nil {
		c.JSON(statusCodeOf(err), errorResponseOf(err))
		return
	}
	resp := newSuccessfulResponse("Get Order details successfully", order)
	c.JSON(http.StatusOK, resp)
}
