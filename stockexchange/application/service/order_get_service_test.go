package service_test

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain/stub"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/out"
	"gitlab.com/briannqc/backend-101/stockexchange/application/service"
	"testing"
)

func TestGetOrderService_GetOrder(t *testing.T) {
	type args struct {
		ctx context.Context
		id  string
	}
	anArgs := args{
		ctx: context.Background(),
		id:  uuid.NewString(),
	}
	type GetOrderPortReturn struct {
		order domain.Order
		err   error
	}
	tests := []struct {
		name               string
		args               args
		authPortErr        error
		getOrderPortReturn GetOrderPortReturn
		want               domain.Order
		wantErr            error
	}{
		{
			name:        "GIVEN authPort returns error THEN forward that error",
			args:        anArgs,
			authPortErr: anErr,
			wantErr:     anErr,
		},
		{
			name: "GIVEN getOrderPort returns error THEN forward that error",
			args: anArgs,
			getOrderPortReturn: GetOrderPortReturn{
				err: anErr,
			},
			wantErr: anErr,
		},
		{
			name: "GIVEN getOrderPort returns an order THEN forward that order",
			args: anArgs,
			getOrderPortReturn: GetOrderPortReturn{
				order: stub.BuyOrderNew(),
			},
			want: stub.BuyOrderNew(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			authPort := out.NewMockCanViewOrderPort(ctrl)
			authPort.EXPECT().
				CanViewOrder(tt.args.ctx, tt.args.id).
				Return(tt.authPortErr).
				Times(1)
			getOrderPort := out.NewMockGetOrderPort(ctrl)
			getOrderPort.EXPECT().
				GetOrderByID(tt.args.ctx, tt.args.id).
				Return(tt.getOrderPortReturn.order, tt.getOrderPortReturn.err).
				MaxTimes(1)

			s := service.NewGetOrderService(authPort, getOrderPort)
			got, err := s.GetOrder(tt.args.ctx, tt.args.id)

			assert.ErrorIs(t, err, tt.wantErr)
			assert.Equal(t, tt.want, got)
		})
	}
}
