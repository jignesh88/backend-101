// Code generated by MockGen. DO NOT EDIT.
// Source: order_book.go

// Package out is a generated GoMock package.
package out

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	domain "gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

// MockGetOrderBookPort is a mock of GetOrderBookPort interface.
type MockGetOrderBookPort struct {
	ctrl     *gomock.Controller
	recorder *MockGetOrderBookPortMockRecorder
}

// MockGetOrderBookPortMockRecorder is the mock recorder for MockGetOrderBookPort.
type MockGetOrderBookPortMockRecorder struct {
	mock *MockGetOrderBookPort
}

// NewMockGetOrderBookPort creates a new mock instance.
func NewMockGetOrderBookPort(ctrl *gomock.Controller) *MockGetOrderBookPort {
	mock := &MockGetOrderBookPort{ctrl: ctrl}
	mock.recorder = &MockGetOrderBookPortMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockGetOrderBookPort) EXPECT() *MockGetOrderBookPortMockRecorder {
	return m.recorder
}

// GetOrderBook mocks base method.
func (m *MockGetOrderBookPort) GetOrderBook(arg0 context.Context) (*domain.OrderBook, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetOrderBook", arg0)
	ret0, _ := ret[0].(*domain.OrderBook)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetOrderBook indicates an expected call of GetOrderBook.
func (mr *MockGetOrderBookPortMockRecorder) GetOrderBook(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetOrderBook", reflect.TypeOf((*MockGetOrderBookPort)(nil).GetOrderBook), arg0)
}
