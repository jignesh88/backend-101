//go:generate mockgen -destination=execution_report_mock.go -package=out -source=execution_report.go

package out

import (
	"context"

	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

type ReportExecutionPort interface {
	ReportExecution(context.Context, ...domain.Order)
}
