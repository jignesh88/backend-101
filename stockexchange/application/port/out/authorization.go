//go:generate mockgen -destination=authorization_mock.go -package=out -source=authorization.go

package out

import (
	"context"
)

type CanViewOrderPort interface {
	CanViewOrder(ctx context.Context, id string) error
}
