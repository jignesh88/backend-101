//go:generate mockgen -destination=order_mock.go -package=in -source=order.go

package in

import (
	"context"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

type PlaceOrderUseCase interface {
	PlaceOrder(ctx context.Context, symbol string, quantity uint, priceCent uint64, side string) (string, error)
}

type GetOrderUseCase interface {
	GetOrder(ctx context.Context, id string) (domain.Order, error)
}
