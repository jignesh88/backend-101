package domain

import "fmt"

type ErrorCategory int

const (
	InvalidInput ErrorCategory = iota
	Unauthorized
	InternalError
)

type Error interface {
	error
	Category() ErrorCategory
	Code() string
	Message() string
	Details() string
}

type err struct {
	category ErrorCategory
	code     string
	message  string
	details  string
}

func (e err) Error() string {
	return fmt.Sprintf("%v: %v", e.code, e.message)
}

func (e err) Category() ErrorCategory {
	return e.category
}

func (e err) Code() string {
	return e.code
}

func (e err) Message() string {
	return e.message
}

func (e err) Details() string {
	return fmt.Sprintf("%v If the issue keeps happening, contact our Customer Support.", e.details)
}

var (
	ErrInvalidStockSymbol Error = err{
		category: InvalidInput,
		code:     "ERR4000",
		message:  "Invalid symbol",
		details:  `Stock symbol must have exactly 4 uppercase character. Please refer our API spec and try again.`,
	}
	ErrInvalidStockCompanyName Error = err{
		category: InvalidInput,
		code:     "ERR4001",
		message:  "Invalid company name",
		details:  `Company name must not be blank. Please refer our API spec and try again.`,
	}
	ErrInvalidOrderQuantity Error = err{
		category: InvalidInput,
		code:     "ERR4010",
		message:  "Invalid order quantity",
		details:  `Order quantity must be more than 0. Please refer our API spec and try again.`,
	}
	ErrInvalidOrderPrice Error = err{
		category: InvalidInput,
		code:     "ERR4011",
		message:  "Invalid order price",
		details:  `Price must be more than 0. Please refer our API spec and try again.`,
	}
	ErrInvalidOrderSide Error = err{
		category: InvalidInput,
		code:     "ERR4012",
		message:  "Invalid order side",
		details:  fmt.Sprintf(`Side must be %v or %v. Please refer our API spec and try again.`, SideBuy, SideSell),
	}
	ErrInvalidOrderStatus Error = err{
		category: InternalError,
		code:     "ERR4012",
		message:  "Invalid order status",
		details:  fmt.Sprintf(`Status must be one of %v or %v. Please refer our API spec and try again.`, StatusNew, StatusFILLED),
	}
)

var (
	ErrCannotViewOrder Error = err{
		category: Unauthorized,
		code:     "ERR4031",
		message:  "Viewing order details is not allowed",
		details:  fmt.Sprintf(`You are not allowed to view the order details, please ensure that your have neccessary privillage.`),
	}
)
