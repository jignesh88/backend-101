package domain

import (
	"github.com/google/uuid"
)

// Cent is the smallest currency unit, used in monetery calculation.
type Cent uint64

// Side is the side of the trade, can only be SideBuy or SideSell.
type Side string

const (
	SideBuy  Side = "BUY"
	SideSell Side = "SELL"
)

type OrderStatus string

const (
	StatusNew    OrderStatus = "NEW"
	StatusFILLED OrderStatus = "FILLED"
)

// GenerateOrderID generates a new id following the business rule,
// which can be assigned to newly created order.
func GenerateOrderID() string {
	return uuid.NewString()
}

type Order struct {
	id       string
	symbol   Symbol
	quantity uint
	price    Cent
	side     Side
	status   OrderStatus
}

// NewOrder validates arguments and only creates Order when these args are correct.
// Creating Order via NewOrder ensures that only valid Order entities exist in the
// application, allowing other components to trust every Order they process.
func NewOrder(id string, symbol string, quantity uint, priceCent uint64, side string, status string) (Order, error) {
	sb, err := NewSymbol(symbol)
	if err != nil {
		return Order{}, err
	}
	if quantity == 0 {
		return Order{}, ErrInvalidOrderQuantity
	}
	if priceCent == 0 {
		return Order{}, ErrInvalidOrderPrice
	}
	if side != string(SideBuy) && side != string(SideSell) {
		return Order{}, ErrInvalidOrderSide
	}
	if status != string(StatusNew) && status != string(StatusFILLED) {
		return Order{}, ErrInvalidOrderStatus
	}
	return Order{
		id:       id,
		symbol:   sb,
		quantity: quantity,
		price:    Cent(priceCent),
		side:     Side(side),
		status:   OrderStatus(status),
	}, nil
}

func (o *Order) ID() string {
	return o.id
}

func (o *Order) Symbol() Symbol {
	return o.symbol
}

func (o *Order) Quantity() uint {
	return o.quantity
}

func (o *Order) Price() Cent {
	return o.price
}

func (o *Order) Side() Side {
	return o.side
}

func (o *Order) SetStatus(status OrderStatus) {
	o.status = status
}

func (o *Order) Status() OrderStatus {
	return o.status
}
