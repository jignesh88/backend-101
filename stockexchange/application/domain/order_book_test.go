package domain_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain/stub"
)

func TestOrderBook_Match(t *testing.T) {
	tests := []struct {
		name             string
		pendingOrders    []domain.Order
		order            domain.Order
		wantFilledOrders []domain.Order
		wantSize         int
	}{
		{
			name:             "GIVEN no pending order THEN no order filled",
			pendingOrders:    []domain.Order{},
			order:            stub.BuyOrderNew(),
			wantFilledOrders: []domain.Order{},
			wantSize:         1,
		},
		{
			name: "GIVEN one sell order pending and new sell THEN no order filled",
			pendingOrders: []domain.Order{
				stub.MustNewOrder(uuid.NewString(), "AAPL", 10, 12300, "SELL", "NEW"),
			},
			order:            stub.SellOrderNew(),
			wantFilledOrders: []domain.Order{},
			wantSize:         2,
		},
		{
			name: "GIVEN one buy order pending and new buy THEN no order filled",
			pendingOrders: []domain.Order{
				stub.MustNewOrder(uuid.NewString(), "AAPL", 10, 12300, "BUY", "NEW"),
			},
			order:            stub.BuyOrderNew(),
			wantFilledOrders: []domain.Order{},
			wantSize:         2,
		},
		{
			name: "GIVEN pending order of other symbol THEN no order filled",
			pendingOrders: []domain.Order{
				stub.MustNewOrder(uuid.NewString(), "AAPL", 10, 12300, "BUY", "NEW"),
			},
			order:            stub.MustNewOrder(uuid.NewString(), "MSFT", 10, 12300, "SELL", "NEW"),
			wantFilledOrders: []domain.Order{},
			wantSize:         2,
		},
		{
			name: "GIVEN pending order of same symbol but different quantity THEN no order filled",
			pendingOrders: []domain.Order{
				stub.MustNewOrder(uuid.NewString(), "AAPL", 10, 12300, "BUY", "NEW"),
			},
			order:            stub.MustNewOrder(uuid.NewString(), "AAPL", 120, 12300, "SELL", "NEW"),
			wantFilledOrders: []domain.Order{},
			wantSize:         2,
		},
		{
			name: "GIVEN pending order of of same symbol, quantity but different price THEN no order filled",
			pendingOrders: []domain.Order{
				stub.MustNewOrder(uuid.NewString(), "AAPL", 10, 10000, "SELL", "NEW"),
			},
			order:            stub.MustNewOrder(uuid.NewString(), "AAPL", 10, 12300, "BUY", "NEW"),
			wantFilledOrders: []domain.Order{},
			wantSize:         2,
		},
		{
			name: "GIVEN a matchable sell order is pending and new buy order THEN both orders filled",
			pendingOrders: []domain.Order{
				stub.BuyOrderNew(),
			},
			order: stub.SellOrderNew(),
			wantFilledOrders: []domain.Order{
				stub.BuyOrderFilled(),
				stub.SellOrderFilled(),
			},
			wantSize: 0,
		},
		{
			name: "GIVEN one of pending order matchable THEN two orders filled",
			pendingOrders: []domain.Order{
				stub.MustNewOrder(uuid.NewString(), "AAPL", 120, 12300, "BUY", "NEW"),
				stub.SellOrderNew(),
				stub.MustNewOrder(uuid.NewString(), "MSFT", 120, 10000, "SELL", "NEW"),
			},
			order: stub.BuyOrderNew(),
			wantFilledOrders: []domain.Order{
				stub.BuyOrderFilled(),
				stub.SellOrderFilled(),
			},
			wantSize: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			orderBook := domain.NewOrderBook()
			for _, order := range tt.pendingOrders {
				filledOrderWhileSeeding := orderBook.Match(order)
				if len(filledOrderWhileSeeding) > 0 {
					assert.Empty(t, filledOrderWhileSeeding, "wrong test data, seeding orders have matched pairs")
					return
				}
			}
			gotFilledOrders := orderBook.Match(tt.order)
			assert.ElementsMatch(t, tt.wantFilledOrders, gotFilledOrders)
			assert.Equal(t, tt.wantSize, orderBook.Size())
		})
	}
}
