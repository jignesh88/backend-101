package domain_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

func TestNewOrder(t *testing.T) {
	tests := []struct {
		name      string
		id        string
		symbol    string
		quantity  uint
		priceCent uint64
		side      string
		status    string
		wantErr   error
	}{
		{
			name:      "GIVEN invalid symbol THEN fails",
			id:        uuid.NewString(),
			symbol:    "WRONG",
			quantity:  100,
			priceCent: 12300,
			side:      "BUY",
			status:    "NEW",
			wantErr:   domain.ErrInvalidStockSymbol,
		},
		{
			name:      "GIVEN quantity is zero THEN fails",
			id:        uuid.NewString(),
			symbol:    "AAPL",
			quantity:  0,
			priceCent: 12300,
			side:      "BUY",
			status:    "NEW",
			wantErr:   domain.ErrInvalidOrderQuantity,
		},
		{
			name:      "GIVEN price is zero THEN fails",
			id:        uuid.NewString(),
			symbol:    "AAPL",
			quantity:  1000,
			priceCent: 0,
			side:      "BUY",
			status:    "NEW",
			wantErr:   domain.ErrInvalidOrderPrice,
		},
		{
			name:      "GIVEN side is neither BUY nor SELL THEN fails",
			id:        uuid.NewString(),
			symbol:    "AAPL",
			quantity:  1000,
			priceCent: 1230,
			side:      "WRONG",
			status:    "FILLED",
			wantErr:   domain.ErrInvalidOrderSide,
		},
		{
			name:      "GIVEN status is not valid THEN fails",
			id:        uuid.NewString(),
			symbol:    "AAPL",
			quantity:  1000,
			priceCent: 1230,
			side:      "BUY",
			status:    "WRONG",
			wantErr:   domain.ErrInvalidOrderStatus,
		},
		{
			name:      "GIVEN args are correct THEN passes",
			id:        uuid.NewString(),
			symbol:    "AAPL",
			quantity:  1000,
			priceCent: 1230,
			side:      "BUY",
			status:    "NEW",
			wantErr:   nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := domain.NewOrder(tt.id, tt.symbol, tt.quantity, tt.priceCent, tt.side, tt.status)
			if tt.wantErr != nil {
				assert.ErrorIs(t, err, tt.wantErr)
			} else {
				_, parseIdErr := uuid.Parse(got.ID())
				assert.NoError(t, parseIdErr)
				assert.Equal(t, tt.symbol, string(got.Symbol()))
				assert.Equal(t, tt.quantity, got.Quantity())
				assert.Equal(t, tt.priceCent, uint64(got.Price()))
				assert.Equal(t, tt.side, string(got.Side()))
				assert.Equal(t, domain.StatusNew, got.Status())
			}
		})
	}
}
