package domain

// OrderBook keeps track of outstanding order and try matching any incoming orders.
type OrderBook struct {
	bids map[Symbol][]Order
	asks map[Symbol][]Order
}

func NewOrderBook() *OrderBook {
	return &OrderBook{
		bids: map[Symbol][]Order{},
		asks: map[Symbol][]Order{},
	}
}

// Match tries to match the incoming order with the others pending in the book. It returns matched orders
// when there is suitable contra order, otherwise it keeps the order in the book and returns nil.
func (ob *OrderBook) Match(order Order) []Order {
	if SideBuy == order.Side() {
		return ob.matchOrder(order, ob.asks, ob.bids)
	}
	return ob.matchOrder(order, ob.bids, ob.asks)
}

func (ob *OrderBook) matchOrder(
	order Order,
	otherSide map[Symbol][]Order,
	sameSide map[Symbol][]Order,
) []Order {
	otherSideOrders, matchSymbol := otherSide[order.Symbol()]
	if !matchSymbol {
		sameSide[order.Symbol()] = append(sameSide[order.Symbol()], order)
		return nil
	}

	for i, contra := range otherSideOrders {
		matchQtyPrice := contra.Quantity() == order.Quantity() && contra.Price() == order.Price()
		if matchQtyPrice {
			order.SetStatus(StatusFILLED)
			contra.SetStatus(StatusFILLED)
			otherSide[order.Symbol()] = append(otherSideOrders[:i], otherSideOrders[i+1:]...)
			return []Order{order, contra}
		}
	}

	sameSide[order.Symbol()] = append(sameSide[order.Symbol()], order)
	return nil
}

// Size returns number of pending orders.
func (ob *OrderBook) Size() int {
	size := 0
	for _, orders := range ob.asks {
		size = size + len(orders)
	}
	for _, orders := range ob.bids {
		size = size + len(orders)
	}
	return size
}
