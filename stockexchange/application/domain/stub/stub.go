// Package stub provides helpful stub factories, designed for test only.
// On exceptional condition, these factories panic with explaination.
package stub

import (
	"encoding/json"
	"log"

	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

func MustNewOrder(id string, symbol string, quantity uint, priceCent uint64, side string, status string) domain.Order {
	order, err := domain.NewOrder(id, symbol, quantity, priceCent, side, status)
	if err != nil {
		log.Panic("MustNewOrder failed with err: ", err)
	}
	return order
}

// SellOrderNew returns a stub sell order at NEW status. The returned order is matchable
// with order created by BuyOrderNew.
func SellOrderNew() domain.Order {
	return MustNewOrder("94f7bcfa-fd38-41bf-92b3-af6689b5400a", "AAPL", 10, 12300, "SELL", string(domain.StatusNew))
}

// SellOrderFilled returns a stub sell order at FILLED status. The returned order the same
// as the one created by SellOrderNew with status changed.
func SellOrderFilled() domain.Order {
	return MustNewOrder("94f7bcfa-fd38-41bf-92b3-af6689b5400a", "AAPL", 10, 12300, "SELL", string(domain.StatusFILLED))
}

// BuyOrderNew returns a stub buy order at NEW status. The returned order is matchable
// with order created by SellOrderNew.
func BuyOrderNew() domain.Order {
	return MustNewOrder("16a9e9fd-d625-4518-99cf-168accc94bf1", "AAPL", 10, 12300, "BUY", string(domain.StatusNew))
}

func BuyOrderNewJSON() string {
	b, err := json.Marshal(BuyOrderNew())
	if err != nil {
		log.Panic("BuyOrderNewJSON failed with err: ", err)
	}
	return string(b)
}

// BuyOrderFilled returns a stub sell order at FILLED status. The returned order the same
// as the one created by BuyOrderNew with status changed.
func BuyOrderFilled() domain.Order {
	return MustNewOrder("16a9e9fd-d625-4518-99cf-168accc94bf1", "AAPL", 10, 12300, "BUY", string(domain.StatusFILLED))
}

// MustOrderBook returns a OrderBook of given NEW orders. These orders
// must not match with one another.
func MustOrderBook(orders ...domain.Order) *domain.OrderBook {
	ob := domain.NewOrderBook()
	for _, o := range orders {
		if o.Status() != domain.StatusNew {
			log.Panicf("Try matching a not 'NEW' order, id: %v, status: %v", o.ID(), o.Status())
		}
		filledOrders := ob.Match(o)
		if len(filledOrders) > 0 {
			log.Panic("wrong test data, seeding orders have matched pairs", filledOrders)
		}
	}
	return ob
}
