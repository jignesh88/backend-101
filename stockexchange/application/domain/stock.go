package domain

import (
	"strings"
	"unicode"
)

type Symbol string

func NewSymbol(symbol string) (Symbol, error) {
	symbol = removeWhitespace(symbol)
	if len(symbol) != 4 {
		return "", ErrInvalidStockSymbol
	}
	if strings.ToUpper(symbol) != symbol {
		return "", ErrInvalidStockSymbol
	}
	return Symbol(symbol), nil
}

func removeWhitespace(s string) string {
	rr := make([]rune, 0, len(s))
	for _, r := range s {
		if !unicode.IsSpace(r) {
			rr = append(rr, r)
		}
	}
	return string(rr)
}

type Stock struct {
	symbol  Symbol
	company string
}

func NewStock(symbol string, company string) (Stock, error) {
	sb, err := NewSymbol(symbol)
	if err != nil {
		return Stock{}, err
	}
	if strings.TrimSpace(company) != company {
		return Stock{}, ErrInvalidStockCompanyName
	}
	return Stock{symbol: sb, company: company}, nil
}

func (s Stock) Symbol() Symbol {
	return s.symbol
}

func (s Stock) Company() string {
	return s.company
}
